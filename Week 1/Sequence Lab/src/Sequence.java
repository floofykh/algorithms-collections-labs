
public class Sequence<E> {
	
	protected E data[];
	protected int size;
	
	/**
	* Initialises this Sequence object to be empty, with an initial
	* capacity of ten elements.
	*/
	@SuppressWarnings("unchecked")
	public Sequence()
	{
		size = 0;
		data = (E[]) new Object[10];
	}
	
	/**
	* Initialises this Sequence object to be empty, with a specified
	* initial capacity.
	*
	* @param capacity - the initial capacity of this Sequence object.
	*
	* @throws IllegalArgumentException - if capacity is non-positive
	*/
	@SuppressWarnings("unchecked")
	public Sequence (int capacity)
	{
		size = 0;
		data = (E[]) new Object[capacity];
	}
	
	/**
	* Returns the number of elements in this Sequence object.
	*
	* @return the number of elements in this Sequence object.
	*/
	public int size ()
	{
		return size;
	}
	
	/**
	* Appends a specified element to this Sequence object.
	*
	* @param element - the element to be inserted at the end of this
	* Sequence object.
	*/
	@SuppressWarnings("unchecked")
	public void append (E element)
	{
		if(size >= data.length)
		{
			//Create the new array
			E[] newArray = (E[]) new Object[data.length+10];
			
			//Copy data from old array to new one
			for(int i=0; i<size; i++)
			{
				newArray[i] = data[i];
			}
			
			//Assign new array to the old one. 
			data = newArray;
		}
		
		//Assign new element to array
		data[size++] = element;
	}
	
	/**
	* Returns the element at a specified index in this Sequence object.
	* The worstTime(n) is constant, where n is the number of elements in
	* this Sequence object.
	*
	* @param k - the index of the element returned.
	*
	* @return the element at index k in this Sequence object.
	*
	* @throws IndexOutOfBoundsException - if k is either negative or
	* greater than or equal to the number of elements in this
	* Sequence object.
	*/
	public E get (int k)
	{
		if(size < k || k < 0)
			throw new IndexOutOfBoundsException();
		
		return data[k];
	}
	
	/**
	* Changes the element at a specified index in this Sequence object.
	* The worstTime(n) is constant, where n is the number of elements in
	* this Sequence object.
	*
	* @param k - the index of the element required.
	* @param newElement - the element to replace the element at index k
	* in this Sequence object.
	*
	* @throws IndexOutOfBoundsException - if k is either negative or
	* greater than or equal to the number of elements in this
	* Sequence object.
	*/
	public void set (int k, E newElement)
	{
		if(size < k || k < 0)
			throw new IndexOutOfBoundsException();
		
		data[k] = newElement;
	}
	
	/**
	* Removes the element at a specified index in this Sequence object
	* and returns it.
	* The worstTime(n) is linear in n, where n is the number of elements
	* in this Sequence object.
	*
	* @param k - the index of the element to be removed.
	*
	* @return the element that was at index k in this Sequence object.
	*
	* @throws IndexOutOfBoundsException - if k is either negative or
	* greater than or equal to the number of elements in this
	* Sequence object.
	*/
	public E remove (int k)
	{
		E retVal = null;
		
		if(k < 0 || k >= size)
			throw new IndexOutOfBoundsException();
		else
		{
			retVal = data[k];
			for(int i=k+1; i<size; i++)
			{
				data[i-1] = data[i];
			}

			size--;
		}
		return retVal;
	}
	/**
	* Returns true if this Sequence object contains one or more elements
	* equal to the given Object, otherwise returns false.
	* The worstTime(n) is linear in n, where n is the number of elements
	* in this Sequence object.
	*
	* @param obj - the object to search for.
	*
	* @return the true if the object is present, false if not.
	*
	*/
	public boolean contains (Object obj)
	{
		for(int i=0; i<size; i++)
		{
			if(data[i] == obj)
				return true;
		}
		return false;
	}

}
