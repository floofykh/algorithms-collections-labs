
public class Sequence<E> {
	
	protected DLLNode first, last;
	protected int size;
	
	/**
	* Initialises this Sequence object to be empty.
	.
	*/
	public Sequence()
	{
		first = null;
		last = null;
		size = 0;
	}
	/**
	* Returns the number of elements in this Sequence object.
	*
	* @return the number of elements in this Sequence object.
	*/
	public int size ()
	{
		return size;
	}
	/**
	* Appends a specified element to this Sequence object.
	*
	* @param element - the element to be inserted at the end of this
	* Sequence object.
	*/
	public void append (E element)
	{
		if(first == null)
		{
			first = new DLLNode();
			first.data = element;
			last = first;
		}
		else 
		{
			DLLNode newNode = new DLLNode();
			newNode.data = element;
			newNode.previous = last;
			last.next = newNode;
			last = newNode;
		}
		
		size++;
	}
	/**
	* Returns the element at a specified index in this Sequence object.
	* The worstTime(n) is linear in n, where n is the number of elements
	* in this Sequence object.
	*
	* @param k - the index of the element returned.
	*
	* @return the element at index k in this Sequence object.
	*
	* @throws IndexOutOfBoundsException - if k is either negative or
	* greater than or equal to the number of elements in this
	* Sequence object.
	*/
	public E get (int k) throws IndexOutOfBoundsException
	{
		E data = null;
		
		if(k>=size)
		{
			throw new IndexOutOfBoundsException();
		}
		else //if (k < size/2)
		{
			DLLNode currentNode = first;
			for(int i=0; i<k; i++)
			{
				currentNode = currentNode.next;
			}
			data = (E) currentNode.data;
		}
		/*else
		{
			DLLNode currentNode = last;
			for(int i=0; i<=size-k; i++)
			{
				currentNode = currentNode.previous;
			}
			data = (E) currentNode.data;
		}*/
		
		return data;
	}
	/**
	* Changes the element at a specified index in this Sequence object.
	* The worstTime(n) is linear in n, where n is the number of elements
	* in this Sequence object.
	*
	* @param k - the index of the element required.
	* @param newElement - the element to replace the element at index k
	* in this Sequence object.
	*Week 3 Laboratory � COMP09044 Page 2
	* @throws IndexOutOfBoundsException - if k is either negative or
	* greater than or equal to the number of elements in this
	* Sequence object.
	*/
	public void set (int k, E newElement)
	{
		if(k < 0 || k >= size)
		{
			throw new IndexOutOfBoundsException();
		}
		else //if (k < size/2)
		{
			DLLNode currentNode = first;
			for(int i=0; i<k; i++)
			{
				currentNode = currentNode.next;
			}
			currentNode.data = newElement;
		}
		/*else
		{
			DLLNode currentNode = last;
			for(int i=size; i>k; i--)
			{
				currentNode = currentNode.previous;
			}
			currentNode.data = newElement;
		}*/
	}
	
	/**
	* Removes the element at a specified index in this Sequence object
	* and returns it.
	* The worstTime(n) is linear in n, where n is the number of elements
	* in this Sequence object.
	*
	* @param k - the index of the element to be removed.
	*
	* @return the element that was at index k in this Sequence object.
	*
	* @throws IndexOutOfBoundsException - if k is either negative or
	* greater than or equal to the number of elements in this
	* Sequence object.
	*/
	public E remove (int k)
	{
		if(k >= size || k < 0)
			throw new IndexOutOfBoundsException();
		else
		{
			DLLNode current = first;
			E retVal = null;
			for(int i=0; i<size; i++)
			{
				if(i==k)
				{
					retVal = current.data;
					current.previous.next = current.next;
					current.next.previous = current.previous;
					size--;
					
					break;
				}
				current = current.next;
			}
			
			return retVal;
		}
			
	}
	/**
	* Returns true if this Sequence object contains one or more elements
	* equal to the given Object, otherwise returns false.
	* The worstTime(n) is linear in n, where n is the number of elements
	* in this Sequence object.
	*
	* @param obj - the object to search for.
	*
	* @return the true if the object is present, false if not.
	*
	*/
	public boolean contains (Object obj)
	{
		DLLNode current = first;
		
		while (current != null)
		{
			if(current.data == obj)
				return true;
			
			current = current.next;
		}
		
		return false;
	}

	
	private class DLLNode
	{
		public E data;
		public DLLNode next, previous;
	}
}
