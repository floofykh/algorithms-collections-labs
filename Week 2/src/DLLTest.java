
public class DLLTest {
	
	private static Sequence<Integer> dllList;
	
	public static void main (String []args)
	{
		initialiseList();
		
		testGet();
		
		testSet();
		
		testContains();

		testRemove();
	}
	
	private static void initialiseList()
	{
		//Initialising list...
		System.out.println("Initialising list...");
		dllList = new Sequence<Integer>();
		
		//Adding elements
		System.out.println("\nAppending 10 Elements (0 - 9)...");
		for(int i=0; i<10; i++)
		{
			long previousTime = System.nanoTime();
			dllList.append(i);
			long dTime = System.nanoTime() - previousTime;
			System.out.print(dTime + "ns -- " );
		}
	}
	
	private static void testGet()
	{
		System.out.println("\n\nTesting Get Method...");
		if(dllList == null)
			System.out.println("List not initialised!!!");
		else
		{
			for(int i=0; i<12; i++)
			{
				System.out.print("Element at " + i + ": ");
				try
				{
					long previousTime = System.nanoTime();
					System.out.print(dllList.get(i));
					long dTime = System.nanoTime() - previousTime;
					System.out.println(" -- " + dTime + "ns" );
				}
				catch(IndexOutOfBoundsException e)
				{
					System.out.println("Index out of bounds");
				}
			}
		}
	}
	
	private static void testSet()
	{
		System.out.println("\n\nTesting Set Method...");
		System.out.println("Set element at 5 to 100");
		try
		{
			long previousTime = System.nanoTime();
			dllList.set(5, 100);
			long dTime = System.nanoTime() - previousTime;
			System.out.println(" -- " + dTime + "ns" );
		}
		catch(IndexOutOfBoundsException e)
		{
			System.out.println("Index out of bounds");
		}
		System.out.println("Set element at 2 to 50");
		try
		{
			long previousTime = System.nanoTime();
			dllList.set(2, 50);
			long dTime = System.nanoTime() - previousTime;
			System.out.println(" -- " + dTime + "ns" );
		}
		catch(IndexOutOfBoundsException e)
		{
			System.out.println("Index out of bounds");
		}
		
		testGet();
	}
	
	private static void testRemove()
	{
		//Check remove method
		System.out.println("\n\nRemoving Elements...");
		try
		{
			long previousTime = System.nanoTime();
			System.out.print(dllList.remove(3));
			long dTime = System.nanoTime() - previousTime;
			System.out.println(" -- " + dTime + "ns" );
		}
		catch(IndexOutOfBoundsException e)
		{
			System.out.println("Index out of bounds");
		}
		try
		{
			long previousTime = System.nanoTime();
			System.out.print(dllList.remove(5));
			long dTime = System.nanoTime() - previousTime;
			System.out.println(" -- " + dTime + "ns" );
		}
		catch(IndexOutOfBoundsException e)
		{
			System.out.println("Index out of bounds");
		}
		try
		{
			long previousTime = System.nanoTime();
			System.out.print(dllList.remove(15));
			long dTime = System.nanoTime() - previousTime;
			System.out.println(" -- " + dTime + "ns" );
		}
		catch(IndexOutOfBoundsException e)
		{
			System.out.println("Index out of bounds");
		}

		System.out.println("Remaining Elements: ");
		for(int i=0; i<dllList.size(); i++)
			System.out.println(dllList.get(i));
	}
	
	private static void testContains()
	{
		//Check Contains method
		System.out.println("\n\nTest Contains Method... \nContains: ");
		for(int i=0; i<15; i+=2)
		{
			long previousTime = System.nanoTime();
			System.out.print(i + ": " + dllList.contains(i));
			long dTime = System.nanoTime() - previousTime;
			System.out.println(" -- " + dTime + "ns" );
		}
	}
}
