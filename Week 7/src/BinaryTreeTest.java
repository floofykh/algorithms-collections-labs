import java.util.Random;


public class BinaryTreeTest {

	public static void main(String[] args) 
	{
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();
		Random randomGen = new Random();
		
		for(int i=0; i<100; i++)
		{
			int randInt = randomGen.nextInt(100);
			bst.add(randInt);
		}
		
		System.out.println("Number of leaves in tree : " + bst.leaves());
	}

}
