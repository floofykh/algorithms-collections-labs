
public class FullTimeEmployee {
	private String name;
	
	public FullTimeEmployee (String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
}
