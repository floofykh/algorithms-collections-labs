import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.HashMap;

@SuppressWarnings("unused")
public class WeekFourMain {

	public static void main(String[] args) 
	{
		//exercise4_1();
		//exercise4_2();
		//exercise4_3();
		//exercise4_4();
		//exercise4_5();
		//exercise4_6();
	}
	
	private static void exercise4_1()
	{
		//Score List
		ArrayList<Integer> scoreList = new ArrayList<Integer>();
		scoreList.add(10);
		scoreList.add(13);
		System.out.println("Score List: \n" + scoreList + "\n");
		
		//Salary List
		LinkedList<Double> salaryList = new LinkedList<Double>();
		salaryList.add(24300.25);
		salaryList.add(216.52);
		System.out.println("Salary List: \n" + salaryList + "\n");
		
		//Word Set
		TreeSet<String> wordSet = new TreeSet<String>();
		wordSet.add("Something.");
		wordSet.add("Something other.");
		System.out.println("Word Set: \n" + scoreList + "\n");
		
		//Employee Set
		HashSet<FullTimeEmployee> employeeSet = new HashSet<FullTimeEmployee>();
		employeeSet.add(new FullTimeEmployee("Jack"));
		employeeSet.add(new FullTimeEmployee("Jill"));
		System.out.println("Employee Set: \n" + employeeSet + "\n");
		
		//Student Map
		TreeMap<String, Double> treeMap = new TreeMap<String, Double>();
		treeMap.put("Carol", 10.1);
		treeMap.put("Frank", 1.9);
		System.out.println("Student Map: \n" + treeMap + "\n");
		
		//Trip Map
		HashMap<String, Double> tripMap = new HashMap<String, Double>();
		tripMap.put("JK922", 3000.6);
		tripMap.put("BE956", 20.6);
		System.out.println("Trip Map: \n" + tripMap + "\n");
	}
	
	private static void exercise4_2()
	{
		ArrayList<String> stringArray = new ArrayList<String>();
		ArrayList<Integer> intArray = new ArrayList<Integer>();
		
		for(int i=0; i<3; i++)
		{
			stringArray.add("The Number " + i);
			intArray.add(i);
		}
		
		stringArray.remove(1);
		intArray.remove(1);
		
		stringArray.add(0, "A New Number!");
		intArray.add(0, 99);
		
		System.out.println("String Array: \n" + stringArray + "\n");
		System.out.println("Integer Array: \n" + intArray + "\n");
	}

	private static void exercise4_3()
	{
		System.out.println("ArrayLists have the ensureCapacity() method.");
		System.out.println("LinkedLists have the descendigIterator() method.");
	}

	private static void exercise4_4()
	{
		/*LinkedList<String> team = new LinkedList<String> ();
		team.add ("Garcia");
		Iterator<String> itr = team.iterator();
		Integer player = itr.next();*/
		
		System.out.println("Code snippet generates Type Mismatch error at compile time.");
	}

	private static void exercise4_5()
	{
		ArrayList<String> team1 = new ArrayList<String>();
		ArrayList<String> team2 = new ArrayList<String>();
		ArrayList<ArrayList<String>> league = new ArrayList<ArrayList<String>>();
		
		for(int i=0; i<3; i++)
		{
			team1.add("Team " + (i+1));
		}
		for(int i=0; i<4; i++)
		{
			team2.add("Team " + (100-i));
		}
		
		league.add(team1);
		league.add(team2);
		
		System.out.println(league);
	}

	private static void exercise4_6()
	{
		System.out.println("Code snippet should print out NULL, and then three of the elements in wordSet.\nThis is because the iterator initially points to nothing.");
		
		TreeSet<String> wordSet = new TreeSet<String>();
		wordSet.add ("super");
		wordSet.add ("swell");
		wordSet.add ("swellegant");
		wordSet.add ("super");
		Iterator<String> itr = wordSet.iterator();
		for (int i = 0; i < 4; i++)
		System.out.println (itr.next());

		System.out.println("Code actually geerates an error because of the reason metioned above.");
	}
}
